<?php
/* Template Name: Blog */
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Dixie_Mechanical_2019
 */

get_header();
?>

<!-- Header -->
<?php get_template_part( 'template-parts/header-section' ); ?>

<!-- Our Work -->
<section id="work">
    <div class="container work">

        <div class="row  no-gutters content justify-content-md-center images mb-0 ">

            <div class="col-lg-12 blog-card">
                <div class="row  no-gutters">
                    <div class="col-lg-8 image">
                        <img src="https://images.unsplash.com/photo-1555570181-c590c39f61fc?ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80" alt="">
                    </div>
                    <div class="col-lg-4 header details">
                        <div class="info">
                            <h2 class="bottom-border-thin">Post Title</h2>
                            <p>01/01/2019 by Doug Killough</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 blog-card">
                <div class="row  no-gutters">
                    <div class="col-lg-8">
                        <img src="https://images.unsplash.com/photo-1555570181-c590c39f61fc?ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80" alt="">
                    </div>
                    <div class="col-lg-4 header details">
                        <div class="info">
                            <h2 class="bottom-border-thin">Post Title</h2>
                            <p>01/01/2019 by Doug Killough</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 blog-card">
                <div class="row  no-gutters">
                    <div class="col-lg-8">
                        <img src="https://images.unsplash.com/photo-1555570181-c590c39f61fc?ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80" alt="">
                    </div>
                    <div class="col-lg-4 header details">
                        <div class="info">
                            <h2 class="bottom-border-thin">Post Title</h2>
                            <p>01/01/2019 by Doug Killough</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>


<?php
get_footer();
