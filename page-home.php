<?php
/* Template Name: Home */
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Dixie_Mechanical_2019
 */


// Vars
$slides = get_field('slider');
$cta = get_field('call_to_action');
$workDescription = get_field('work_description');
$services = get_field('services');
$about = get_field('about');
$map = get_field('map');
get_header();
?>

<!-- Carousel -->
<section id="carousel">
  <div id="myCarousel" class="carousel slide carousel-fade bottom-border" data-ride="carousel">
	  <ol class="carousel-indicators">
          <?php $count = 0; foreach($slides as $slide){?>
              <?php if(count($slides) > 1) {?>
                  <li data-target="#myCarousel" data-slide-to="<?php echo $count; ?>" class="<?php if ($count == 0) { echo 'active'; } ?>"></li>
              <?php } ?>
          <?php $count++;} ?>
	  </ol>
	  <div class="carousel-inner">

          <?php $count = 0; foreach($slides as $slide){?>

              <div class="carousel-item <?php if(count($slides) > 1 && $count == 0) { echo 'active'; } elseif (count($slides) == 1) { echo 'active'; }?>">

                  <?php if($slide['background']['type'] == 'video'){ ?>
                      <video src="<?php echo $slide['background']['url']; ?>" autoplay controls>

                      </video>
              <?php } elseif($slide['background']['type'] == 'image'){ ?>
                  <img class="" src="<?php echo $slide['background']['url']; ?>" alt="<?php if($slide['background']['alt']){echo $slide['background']['alt'];} else { echo 'Slider image'; } ?>">
              <?php } ?>
                <div class="container">
                  <div class="carousel-caption text-center">
                    <?php if($slide['header']){?>
                        <h1><?php echo $slide['header']; ?></h1>
                    <?php } ?>
                    <?php if($slide['content']){?>
                        <p><?php echo $slide['content']; ?></p>
                    <?php } ?>
                  </div>
                </div>
              </div>

          <?php $count++; } ?>
	  </div>
      <?php if(count($slides) > 1){?>
	  <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
		<span class="carousel-control-prev-icon" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	  </a>
	  <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
		<span class="carousel-control-next-icon" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	  </a>
  <?php } ?>
	</div>
</section>

<!-- Call To Action -->
<section id="cta-card">
	<div class="container-fluid cta-card">
		<div class="row justify-content-md-center">
			<div class="col-lg-4">
				<h2><?php echo $cta['header'] ?></h2>
				<p><?php echo $cta['content'] ?></p>
			</div>
			<div class="col-lg-2 button">
				<a href="<?php echo $cta['link'] ?>"><span class="btn btn-primary btn-cta"><?php echo $cta['button'] ?></span></a>
			</div>
		</div>
	</div>
</section>


<!-- Services -->
<section id="services" class="bg-alt">
    <div class="container services">
        <div class="row header justify-content-md-center">
            <h1 class="bottom-border-thin">Our Services</h1>
        </div>

        <div class="row content justify-content-md-center">
            <div class="col-lg-10">
                <?php if($services['description']) {?>
                <div class="description"><?php echo $services['description'] ?></div>
            <?php } ?>
            </div>

            <?php $query = new WP_Query(array(
                'post_type' => 'service',
                'post_status' => 'publish',
                'posts_per_page' => 3
            ));


            while ($query->have_posts()) {
                $query->the_post();
            ?>
            <div class="col-lg-4 service">
                <img src="<?php echo get_field('image')['url']; ?>" alt="<?php echo get_field('image')['alt']; ?>">
                <h2><?php echo the_field('header'); ?></h2>
            </div>
            <?php
        }

        wp_reset_query(); ?>

        </div>
        <div class="row view-all justify-content-md-center">
            <a href="/services"><span class="btn btn-primary">View All</span></a>
        </div>
    </div>
</section>


<!-- Our Work -->
<section id="work">
	<div class="container work">
		<div class="row header justify-content-md-center">
			<h1 class="bottom-border-thin">Our Work</h1>
		</div>
		<div class="row content justify-content-md-center">
			<div class="col-lg-10">
                <?php if($workDescription) {?>
				<div class="description"><?php echo $workDescription ?></div>
            <?php } ?>
			</div>
			<div class="images justify-content-md-center">

                <?php $query = new WP_Query(array(
                    'post_type' => 'work',
                    'post_status' => 'publish',
                    'posts_per_page' => 4
                ));


                while ($query->have_posts()) {
                    $query->the_post();
                ?>
                <div class="col-lg-3">
                    <a href="<?php echo get_permalink(); ?>"><img src="<?php echo the_post_thumbnail_url(); ?>" alt="<?php echo the_title(); ?>">
                    <div class="work-overlay">
                        <p class="m-0"><?php echo the_title(); ?></p>
                    </div>
                    </a>
                </div>
                <?php
            }

            wp_reset_query(); ?>





			</div>
		</div>
		<div class="row view-all justify-content-md-center">
            <a href="work"><span class="btn btn-primary">View All</span></a>
		</div>
	</div>
</section>

<!-- Clients -->
<section id="clients" class="bg-alt">
	<div class="slider">

        <?php $query = new WP_Query(array(
            'post_type' => 'client',
            'post_status' => 'publish',
            'posts_per_page' => -1
        ));


        while ($query->have_posts()) {
            $query->the_post();
        ?>
        <div class="slide"><img src="<?php echo the_field('logo'); ?>" alt="<?php echo the_title(); ?>"></div>

        <?php
    }

    wp_reset_query(); ?>

	</div>
</section>

<!-- About Us -->
<section id="about">
	<div class="container about-us">
		<div class="row header justify-content-md-center">
			<h1 class="bottom-border-thin"><?php echo $about['header']; ?></h1>
		</div>
        <div class="row content justify-content-md-center">
			<div class="col-lg-10">
				<div><?php echo $about['description']; ?></div>
			</div>
			<div class="col-lg-6 main">
				<img src="<?php echo $about['image']['url']; ?>" alt="<?php echo $about['image']['alt']; ?>">
			</div>
			<div class="col-lg-6 highlights">
				<div class="row">

                    <?php foreach($about['highlights'] as $highlight) {?>

    					<div class="col-lg-12 highlight">
    						<div class="row">
    							<div class="col-lg-2 image">
    								<img src="<?php echo $highlight['image']['url']; ?>" alt="<?php echo $highlight['image']['alt']; ?>">
    							</div>
    							<div class="col-lg-10 description">
    								<p><?php echo $highlight['description']; ?></p>
    							</div>
    						</div>
    					</div>

                    <?php } ?>

				</div>
			</div>

		</div>
        <div class="row view-all about justify-content-md-center">
            <a href="/about"><span class="btn btn-primary">View More</span></a>
        </div>

	</div>
</section>

<!-- Google Map -->
<section id="map" class="main">
	<div class="container-fluid p-0">
        <?php echo $map;  ?>
	</div>
</section>

<!-- Contact Us -->
<section id="contact">
	<div class="container contact">
		<div class="row header justify-content-md-center">
			<h1 class="bottom-border-thin">Contact Us</h1>
		</div>
		<div class="row content justify-content-md-center">
			<div class="col-lg-8">
	             <?php echo do_shortcode('[contact-form-7 id="105" title="Main"]') ?>
			</div>
		</div>
	</div>
</section>




<?php
get_footer();
