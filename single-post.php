<?php
/* Template Name: Post
 * Template Post Type: post
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Dixie_Mechanical_2019
 */

get_header();
?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<!-- Header -->
<section id="header" class="bottom-border">
    <img src="<?php echo the_post_thumbnail_url(); ?>" alt="<?php echo get_the_post_thumbnail_caption(); ?>">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-lg-4 col-md-6 col-sm-8 card blog">
                <h1 class="mb-0"><?php echo the_title(); ?></h1>
                <p>by <?php echo the_author_firstname(); echo ' '; echo the_author_lastname(); ?></p>
                <p><?php echo get_the_date(); ?></p>
            </div>
        </div>
    </div>
</section>

<!-- Our Work -->
<section id="blog">
    <div class="container work">

        <div class="row content justify-content-md-center images mb-0">
            <div class="col-lg-10">
                <p class="mb-0"><?php echo the_content(); ?></p>
            </div>
        </div>
    </div>
</section>


<?php
endwhile;
endif;
get_footer();
