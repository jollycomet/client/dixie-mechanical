<?php
/* Template Name: Contact */
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Dixie_Mechanical_2019
 */

get_header();
?>

<!-- Header -->
<?php get_template_part( 'template-parts/header-section' ); ?>

<?php $map = get_field('map');
$address = get_field('address');?>

<!-- Our Work -->
<section id="work">
    <div class="container work">

        <div class="row content justify-content-md-center images mb-0">
            <div class="col-lg-6">
                <!-- Google Map -->
                <section id="map-contact" class="">
                    <div class="p-0">
                         <?php echo $map ?>
                    </div>
                </section>
            </div>
            <div class="col-lg-6">
                <!-- Contact Us -->
                <section id="">
                    <div class="container contact">
                        <div class="row header justify-content-md-center">
                            <h2><?php echo $address ?> </h2>
                        </div>
                        <div class="row content justify-content-md-center">
                            <div class="col-lg-12">
                                <?php echo do_shortcode('[contact-form-7 id="105" title="Main"]') ?>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</section>


<?php
get_footer();
