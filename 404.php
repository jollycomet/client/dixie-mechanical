<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Dixie_Mechanical_2019
 */

get_header();
?>

<section id='error'>
	<div class="content">
		<h1>Error 404.</h1>
		<p>The page you are looking for cannot be found. <br>
		Please check the url and try again. </p>
	</div>
</section>





<?php
get_footer();
