<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Dixie_Mechanical_2019
 */

$footer = get_field('footer', 'option');
?>

<!-- Footer -->
<footer id="footer">
	<div class="container footer">
		<div class="row content">
			<div class="col-lg-4">
				<h3 class="header"><?php echo $footer['about']['header']; ?></h3>
				<p><?php echo $footer['about']['content']; ?></p>
			</div>
			<div class="col-lg-4">
				<h3 class="header"><?php echo $footer['services']['header']; ?></h3>
				<p><?php echo $footer['services']['content']; ?></p>
			</div>
			<div class="col-lg-4">
				<h3 class="header"><?php echo $footer['find_us']['header']; ?></h3>
				<div class="social-links">
					<?php foreach ($footer['find_us']['social'] as $social) {?>
						<a target="_blank" href="<?php echo $social['link']; ?>"><img src="<?php echo $social['social_icon']['url']; ?>" alt="<?php echo $social['social_icon']['alt']; ?>"/></a>
					<?php } ?>
				</div>
				<p class="contact"><?php echo $footer['find_us']['phone']; ?></p>
				<p class="contact"><?php echo $footer['find_us']['address']; ?></p>
			</div>
		</div>
		<div class="row justify-content-md-center copyright">
			<div class="col-lg-12">
				<p>© Dixie Mechanical Inc <?php echo date("Y"); ?></p>
			</div>
		</div>
	</div>
</footer>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://maps.googleapis.com/maps/api/js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.js"></script>

<!-- Slider -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>

<script type="text/javascript">




/* Open */
function openNav() {
  document.getElementById("myNav").style.height = "100%";
}

/* Close */
function closeNav() {
  document.getElementById("myNav").style.height = "0%";
}



$(document).on('click', '[data-toggle="lightbox"]', function(event) {
		event.preventDefault();
		$(this).ekkoLightbox({
			showArrows: true
		});
	});


	document.body.style.setProperty('--main-color','<?php if( get_field('main_color', 'option') ) {echo get_field('main_color', 'option');} else { echo '#b40100'; } ?>')
	document.body.style.setProperty('--main-color-transparent','<?php if( get_field('main_color', 'option') ) {echo get_field('main_color', 'option').'80';} else { echo '#b4010080'; } ?>')

	$(document).ready(function(){
	    $('.slider').slick({
	        slidesToShow: <?php if( get_field('slider_count', 'option') ) {echo get_field('slider_count', 'option');} else { echo '5'; } ?>,
	        centerMode: true,
	        slidesToScroll: 1,
	        autoplay: true,
	        autoplaySpeed: 1500,
	        arrows: false,
	        dots: false,
	        pauseOnHover: false,
	        responsive: [{
	            breakpoint: 768,
	            settings: {
	                slidesToShow: 4
	            }
	        }, {
	            breakpoint: 520,
	            settings: {
	                slidesToShow: 3
	            }
	        }]
	    });
	});
</script>


<?php wp_footer(); ?>

</body>
</html>