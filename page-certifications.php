<?php
/* Template Name: Certifications */
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Dixie_Mechanical_2019
 */

get_header();
?>

<!-- Header -->
<?php get_template_part( 'template-parts/header-section' ); ?>

<!-- Our Work -->
<section id="work">
    <div class="container work">

        <div class="row content justify-content-md-center images">
            <?php $query = new WP_Query(array(
                'post_type' => 'certification',
                'post_status' => 'publish',
                'posts_per_page' => -1
            ));


            while ($query->have_posts()) {
                $query->the_post();
            ?>
            <div class="col-lg-3">
                <a href="<?php echo get_permalink(); ?>"><img src="<?php echo the_post_thumbnail_url(); ?>" alt="<?php echo get_the_post_thumbnail_caption(); ?>">
                <div class="work-overlay">
                    <p class="m-0"><?php echo the_title(); ?></p>
                </div>
                </a>
            </div>
            <?php
        }

        wp_reset_query(); ?>
        </div>
    </div>
</section>


<?php
get_footer();
