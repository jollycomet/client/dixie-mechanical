<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Dixie_Mechanical_2019
 */

$favicon = get_field('favicon', 'option');
$logo = get_field('logo', 'option');
$analytics = get_field('analytics', 'option');


?>


<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php bloginfo("template_directory"); ?>/main.css">
	<link rel="stylesheet" href="<?php bloginfo("template_directory"); ?>/slider.css">
	<link rel="shortcut icon" href="<?php if($favicon){ echo $favicon; } ?>" type="image/x-icon" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css">


	<?php wp_head(); ?>
	<?php if(is_user_logged_in()) {?>
		<style media="screen">
			#nav {
				top: 15px;
			}
		</style>
	<?php } ?>
</head>

<?php foreach($analytics as $code) {
	echo $code['snippet'];
}?>

<body <?php body_class(); ?>>
	<header>
		<section id="nav">
		</section>
		<a href="/"><img class="logo" src="<?php if($logo){ echo $logo; } ?>" alt="Logo"></a>
		<span class="hamburger" onclick="openNav()"></span>

		<div id="myNav" class="overlay">

		  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

		  <nav class="overlay-content">
			<ul class="link"><?php wp_nav_menu(
		     array(
		        'menu' => 'Main Navigation',
				'items_wrap'=>'%3$s',
   				'container' => false
		     )
		 ); ?></ul>
		</nav>

		</div>
	</header>
