<?php
/* Template Name: Services */
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Dixie_Mechanical_2019
 */

get_header();
?>

<!-- Header -->
<?php get_template_part( 'template-parts/header-section' ); ?>

<!-- Services -->
<section id="services">
    <div class="container services">

        <div class="row content justify-content-sm-center">
            <?php $query = new WP_Query(array(
                'post_type' => 'service',
                'post_status' => 'publish',
                'posts_per_page' => 3
            ));


            while ($query->have_posts()) {
                $query->the_post();
            ?>
            <div class="col-lg-4 service">
                <img src="<?php echo get_field('image')['url']; ?>" alt="<?php echo get_field('image')['alt']; ?>">
                <h2><?php echo the_field('header'); ?></h2>
                <div><?php echo the_field('description'); ?></div>
            </div>
            <?php
        }

        wp_reset_query(); ?>
        </div>
    </div>
</section>


<?php
get_footer();
