<?php
/* Template Name: Work
 * Template Post Type: work
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Dixie_Mechanical_2019
 */


 $details = get_field('details');
 $gallery = get_field('gallery');
 $video = get_field('video');

 get_header();
 ?>
 <?php
 while ( have_posts() ) :
     the_post(); ?>

<!-- Header -->
<?php get_template_part( 'template-parts/header-section' ); ?>


<!-- Our Work -->
<section id="work">
    <div class="container work">
        <div class="row project mb-20">
            <div class="col-lg-4 details">
                <?php foreach ($details['info'] as $info) {?>
                    <p> <b><?php echo $info['section'] ?>:</b> <?php echo $info['entry'] ?></p>
                <?php } ?>
            </div>
            <div class="col-lg-8">
                <?php echo $details['description'] ?>
            </div>
        </div>

        <?php if($video) {?>
                <div class="row youtube">
                    <div class="col">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?php echo explode('v=', $video)[1] ?>" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
        <?php } ?>

        <?php if($gallery) {?>
        <div class="row content justify-content-md-center images">

            <?php foreach($gallery as $image) {?>
            <div class="col-lg-3 mb-20">
                <a href="<?php echo $image['url']; ?>" data-toggle="lightbox" data-gallery="gallery-work">
                    <div class="work-container">
                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="">
                    </div>
                </a>
            </div>
        <?php } ?>
        </div>

    <?php } ?>




    </div>
</section>


<?php
endwhile;
get_footer();
