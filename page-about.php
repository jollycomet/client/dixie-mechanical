<?php
/* Template Name: About */
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Dixie_Mechanical_2019
 */

get_header();
?>
<?php
while ( have_posts() ) :
    the_post(); ?>
<!-- Header -->
<?php get_template_part( 'template-parts/header-section' ); ?>

<!-- Content -->
<section id="content">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-lg-10">


        			<?php the_content(); ?>

            </div>
        </div>
    </div>
</section>


<?php

endwhile; // End of the loop.
get_footer();
