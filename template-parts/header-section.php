<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Dixie_Mechanical_2019
 */

?>

<!-- Header -->
<section id="header" class="bottom-border">
	<img src="<?php echo the_post_thumbnail_url(); ?>" alt="<?php echo get_the_post_thumbnail_caption(); ?>">
	<div class="container">
		<div class="row justify-content-md-center">
			<div class="col-lg-4 col-md-6 col-sm-8 card">
				<h1 class="mb-0"><?php echo the_title(); ?></h1>
			</div>
		</div>
	</div>
</section>
