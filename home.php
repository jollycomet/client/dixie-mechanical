<?php
/* Template Name: Blog */
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Dixie_Mechanical_2019
 */

get_header();
?>


<?php $query = new WP_Query(array(
    'post_type' => 'post',
    'post_status' => 'publish',
    'posts_per_page' => 1
));


while ($query->have_posts()) {
    $query->the_post();
?>
<!-- Header -->
<a href="<?php echo get_permalink(); ?>">
    <section id="header" class="bottom-border blog">
    	<img src="<?php echo the_post_thumbnail_url(); ?>" alt="<?php echo get_the_post_thumbnail_caption(); ?>">
    	<div class="container">
    		<div class="row justify-content-md-center">
    			<div class="col-lg-4 col-md-6 col-sm-8 card">
    				<h1 class="mb-0"><?php echo the_title(); ?></h1>
                    <p>Latest Post</p>
    			</div>
    		</div>
    	</div>
    </section>
</a>

<?php
}

wp_reset_query(); ?>


<!-- Our Work -->
<section id="work">
    <div class="container work">

        <div class="row  no-gutters content justify-content-md-center images mb-0 ">


            <?php $query = new WP_Query(array(
                'post_type' => 'post',
                'post_status' => 'publish',
                'posts_per_page' => 10,
                'offset' => 1
            ));


            while ($query->have_posts()) {
                $query->the_post();
            ?>
            <a class="post-link" href="<?php echo the_permalink(); ?>">
                <div class="col-lg-12 blog-card">
                    <div class="row  no-gutters">
                        <div class="col-lg-8 image">
                            <img src="<?php echo the_post_thumbnail_url(); ?>" alt="<?php echo get_the_post_thumbnail_caption(); ?>">
                        </div>
                        <div class="col-lg-4 header details">
                            <div class="info">
                                <h2 class="bottom-border-thin"><?php echo the_title(); ?></h2>
                                <p><?php echo get_the_date(); ?> by <?php echo the_author_firstname();echo ' '; echo the_author_lastname(); ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </a>

            <?php
            }

            wp_reset_query(); ?>

        </div>
    </div>
</section>


<?php
get_footer();
